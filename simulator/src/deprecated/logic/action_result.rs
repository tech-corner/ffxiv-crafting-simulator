use crate::ffxiv::enums::crafting_action::CraftingAction;
use crate::ffxiv::enums::fail_cause::FailCause;
use crate::ffxiv::logic::craft_simulation::SimulationState;

#[derive(PartialEq, Copy, Clone)]
pub enum ActionStatus {
    Succeeded,
    Failed,
    Skipped,
}

/// Stat difference after action
pub struct StatDiff {
    // Amount of progression added to the craft
    pub progression: i16,
    // Amount of quality added to the craft
    pub quality: i16,
    // CP added to the craft (negative if removed)
    pub cp: i16,
    // Solidity added to the craft (negative if removed)
    pub durability: i16,
}

impl StatDiff {
    pub fn new(state: &SimulationState) -> StatDiff {
        StatDiff {
            progression: state.progression as i16,
            quality: state.quality as i16,
            cp: state.available_cp as i16,
            durability: state.durability as i16,
        }
    }
    pub fn calc_diff(&mut self, state: &SimulationState) {
        self.progression = state.progression as i16 - self.progression;
        self.quality = state.quality as i16 - self.quality;
        self.cp = state.available_cp as i16 - self.cp;
        self.durability = state.durability as i16 - self.durability;
    }
}

pub struct ActionResult {
    pub action: CraftingAction,
    pub status: ActionStatus,
    pub fail_cause: Option<FailCause>,
    pub action_stat_diff: StatDiff,
    pub buff_stat_diff: Option<StatDiff>,
}
