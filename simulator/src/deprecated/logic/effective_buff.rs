use crate::ffxiv::enums::buff::Buff;
use crate::ffxiv::logic::craft_simulation::{CraftSimulation, SimulationState};
pub struct EffectiveBuff {
    pub duration: u16,
    pub stacks: u8,
    pub buff: Buff,
    pub applied_step: u8,
    pub tick: Option<fn(simulation: &CraftSimulation, state: &mut SimulationState)>,
    pub on_expire: Option<
        fn(simulation: &CraftSimulation, state: &mut SimulationState) -> Option<EffectiveBuff>,
    >,
}
