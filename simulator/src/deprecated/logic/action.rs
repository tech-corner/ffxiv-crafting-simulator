use crate::ffxiv::logic::craft_simulation::SimulationState;
use crate::{
    ffxiv::enums::crafting_action::CraftingAction, ffxiv::enums::fail_cause::FailCause,
    ffxiv::logic::craft_simulation::CraftSimulation,
};

// pub struct SomeQualityAction {
//     quality_action: QualityAction,
// }
//
// pub struct QualityAction {
//     general_action: GeneralAction,
// }
// pub struct GeneralAction {
//     action: Action,
// }
// pub struct Action {}
//
// impl Action {
//     // Implementations
//     fn get_action_success_rate(action: &dyn ActionLogic, simulation: &CraftSimulation) -> u8 {
//         let state = simulation.state.borrow();
//         let base_rate = action.get_success_rate(simulation);
//         if state.step_state == StepState::Centered {
//             return base_rate + 25;
//         }
//         return base_rate;
//     }
// }
//
// impl ActionLogic for Action {
//     fn will_fail(&self, simulation: &CraftSimulation) -> Option<FailCause> {
//         if simulation.safe_mode && Action::get_action_success_rate(self, simulation) < 100 {
//             return Option::Some(FailCause::UnsafeAction);
//         }
//
//         let (reqClass, reqLevel) = self.get_level_requirement(simulation);
//         let controlRequirement = simulation.get_recipe().controlRequired;
//
//         Option::None
//     }
//
//     fn execute(&self, simulation: &CraftSimulation) -> ActionResult {
//         // TODO actually execute the action
//         let state = simulation.state.borrow_mut();
//         let steps = state.get_steps().borrow_mut();
//         ActionResult {
//             action: self.get_info(),
//         }
//     }
// }

// pub trait ActionInfo {
//     fn get_level_requirement(&self, simulation: &CraftSimulation);
//     fn get_success_rate(&self, simulation: &CraftSimulation);
// }

/// Crafting action logic
pub trait Action {
    /// Get exact action type
    fn get_type(&self) -> CraftingAction;

    fn is(&self, action_type: CraftingAction) -> bool {
        self.get_type() == action_type
    }

    fn success_rate(&self, sim: &CraftSimulation, state: &SimulationState) -> u8;

    fn requires_good(&self) -> bool;

    fn get_durability_cost(&self, sim: &CraftSimulation, state: &SimulationState) -> u16;

    fn get_cp_cost(&self, sim: &CraftSimulation, state: &SimulationState) -> u16;

    /// Check if action can succeed and if it is not - provide reason of that
    fn will_fail(&self, sim: &CraftSimulation, state: &SimulationState) -> Option<FailCause>;

    /// If an action is skipped on fail, it doesn't tick buffs.
    /// Example: Observe, Master's Mend, buffs.
    fn skip_on_fail(&self) -> bool;

    ///
    fn skip_buff_ticks(&self) -> bool;

    /// Execute the action
    fn execute(&self, sim: &CraftSimulation, state: &mut SimulationState);

    ///
    fn on_fail(&self, sim: &CraftSimulation, state: &mut SimulationState);
}
