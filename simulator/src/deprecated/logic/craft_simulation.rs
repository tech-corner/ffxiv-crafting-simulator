use crate::ffxiv::craft::{CrafterStats, Recipe};
use crate::ffxiv::enums::buff::Buff;
use crate::ffxiv::enums::crafting_action::CraftingAction;
use crate::ffxiv::enums::fail_cause::FailCause;
use crate::ffxiv::enums::step_state::StepState;
use crate::ffxiv::logic::action::Action;
use crate::ffxiv::logic::action_result::{ActionResult, ActionStatus, StatDiff};
use crate::ffxiv::logic::effective_buff::EffectiveBuff;
use crate::ffxiv::tables::hq::HQ_TABLE;
use rand::Rng;
use std::borrow::Borrow;
use std::cell::Cell;

pub struct CraftResult {
    pub hq_percent: u8,
    pub success: bool,
    pub fail_cause: Option<FailCause>,
}

#[derive(PartialEq, Copy, Clone)]
pub enum CraftState {
    InProgress,
    Failed,
    Succeeded,
}

pub struct SimulationState {
    pub progression: u16,
    pub quality: u16,
    pub durability: u16,
    pub available_cp: u16,

    pub steps: Vec<ActionResult>,
    pub craft_state: CraftState,
    pub current_step_state: StepState,

    buffs: Cell<Vec<EffectiveBuff>>,
}

impl SimulationState {
    pub fn repair(&mut self, recipe: &Recipe, amount: u16) {
        self.durability += amount;
        if self.durability > recipe.durability {
            self.durability = recipe.durability;
        }
    }
}

/// FFXIV Craft simulation config
pub struct CraftSimulation {
    pub recipe: Recipe,
    pub crafter_stats: CrafterStats,
    pub linear: bool,
    pub safe_mode: bool,

    actions: Vec<Box<dyn Action>>,
}

impl CraftSimulation {
    /// Create uninitialized simulation engine
    fn new(recipe: Recipe, crafter: CrafterStats) -> CraftSimulation {
        CraftSimulation {
            recipe,
            crafter_stats: crafter,
            actions: Vec::new(),
            linear: false,
            safe_mode: false,
        }
    }

    // ===== Logic =====
    /// Execute the simulation
    fn run(&self) -> CraftResult {
        let mut state = SimulationState {
            progression: 0,
            quality: 0,
            durability: 0,
            available_cp: 0,
            steps: Vec::new(),
            buffs: Cell::new(Vec::new()),
            craft_state: CraftState::InProgress,
            current_step_state: StepState::Normal,
        };
        let mut fail: Option<FailCause> = Option::None;
        for action in self.actions.iter() {
            let mut result = self.run_action(action, &mut state);
            if fail.is_none() {
                fail = result.fail_cause;
            }

            let mut stats_diff = StatDiff::new(&state);
            let skip_ticks_on_fail =
                result.status != ActionStatus::Succeeded && action.skip_on_fail();

            if state.craft_state == CraftState::InProgress
                && !action.skip_buff_ticks()
                && !skip_ticks_on_fail
            {
                // Tick buffs after checking synth result, so if we reach 0 durability, synth fails.
                self.tick_buffs(&mut state);
            }

            stats_diff.calc_diff(&state);
            result.buff_stat_diff = Option::Some(stats_diff);

            // Tick state to change it for next turn if not in linear mode
            if !self.linear
                && !action.is(CraftingAction::FinalAppraisal)
                && !action.is(CraftingAction::RemoveFinalAppraisal)
            {
                self.tick_state(&mut state);
            }

            state.steps.push(result);
        }

        let rez = CraftResult {
            hq_percent: self.get_hq_percent(&mut state),
            success: state.progression >= self.recipe.progress,
            fail_cause: fail,
        };
        rez
    }

    fn run_action(&self, action: &Box<dyn Action>, state: &mut SimulationState) -> ActionResult {
        let mut fail_cause = action.will_fail(self, state);

        if state.craft_state != CraftState::InProgress || fail_cause.is_some() {
            return ActionResult {
                fail_cause,
                action_stat_diff: StatDiff {
                    cp: 0,
                    progression: 0,
                    quality: 0,
                    durability: 0,
                },
                action: action.get_type(),
                status: match state.craft_state {
                    CraftState::InProgress => ActionStatus::Failed,
                    _ => ActionStatus::Skipped,
                },
                buff_stat_diff: Option::None,
            };
        }

        let mut rnd = rand::thread_rng();
        // The roll for the current action's success rate, 0 if ideal mode, as 0 will even match a 1% chances.
        let probability_roll = match self.linear {
            true => 0,
            false => rnd.gen_range(0, 100),
        };

        let mut stat_diff = StatDiff::new(state);
        let action_success_rate = action.success_rate(self, state);
        let mut success = ActionStatus::Failed;

        if self.safe_mode && (action_success_rate < 100 || action.requires_good()) {
            fail_cause = Option::Some(FailCause::UnsafeAction);
            action.on_fail(self, state);
        } else {
            if action_success_rate >= probability_roll {
                action.execute(self, state);
                success = ActionStatus::Succeeded;
            } else {
                action.on_fail(self, state);
            }
        }

        let durability_cost = action.get_durability_cost(self, state);
        let cp_cost = action.get_cp_cost(self, state);

        // Even if the action failed, we have to remove the durability cost
        state.durability -= durability_cost;
        // Even if the action failed, CP has to be consumed too
        state.available_cp -= cp_cost;

        if state.progression >= self.recipe.progress {
            state.craft_state = CraftState::Succeeded;
        } else if state.durability <= 0 {
            fail_cause = Option::Some(FailCause::DurabilityReachedZero);
            // Check durability to see if the craft is failed or not
            state.craft_state = CraftState::Failed;
        }

        stat_diff.calc_diff(state);
        ActionResult {
            fail_cause,
            action: action.get_type(),
            status: success,
            action_stat_diff: stat_diff,
            buff_stat_diff: Option::None,
        }
    }

    fn tick_buffs(&self, state: &mut SimulationState) {
        // Detach buffs from state
        let mut buffs = state.buffs.replace(Vec::with_capacity(0));

        let mut new_buffs: Vec<EffectiveBuff> = Vec::new();
        for mut buff in buffs.iter_mut() {
            if buff.applied_step < state.steps.len() as u8 {
                // If the buff has something to do, let it do it
                if buff.tick.is_some() {
                    buff.tick.unwrap()(self, state);
                }
                buff.duration -= 1;
            }
            if buff.duration <= 0 && buff.on_expire.is_some() {
                let new = buff.on_expire.unwrap()(self, state);
                if new.is_some() {
                    new_buffs.push(new.unwrap());
                }
            }
        }

        buffs.retain(|buff| buff.duration <= 0);
        buffs.extend(new_buffs);

        // Put buffs back
        state.buffs.replace(buffs);
    }

    ///
    /// Changes the state of the craft,
    /// source: https://github.com/Ermad/ffxiv-craft-opt-web/blob/master/app/js/ffxivcraftmodel.js#L255
    ///
    fn tick_state(&self, state: &mut SimulationState) {
        // If current state is EXCELLENT, then next one is poor
        if state.current_step_state == StepState::Excellent {
            state.current_step_state = StepState::Poor;
            return;
        }

        // Good
        let recipe_level = self.recipe.rlvl;
        let quality_assurance = self.crafter_stats.level >= 63;
        let good_chances: f64;
        if recipe_level >= 300 {
            // 70*+
            good_chances = if quality_assurance { 0.11 } else { 0.1 };
        } else if recipe_level >= 276 {
            // 65+
            good_chances = if quality_assurance { 0.17 } else { 0.15 };
        } else if recipe_level >= 255 {
            // 61+
            good_chances = if quality_assurance { 0.22 } else { 0.2 };
        } else if recipe_level >= 150 {
            // 60+
            good_chances = if quality_assurance { 0.11 } else { 0.1 };
        } else if recipe_level >= 136 {
            // 55+
            good_chances = if quality_assurance { 0.17 } else { 0.15 };
        } else {
            good_chances = if quality_assurance { 0.27 } else { 0.25 };
        }

        // Excellent
        let excellent_chances: f64;
        if recipe_level >= 300 {
            // 70*+
            excellent_chances = 0.01;
        } else if recipe_level >= 255 {
            // 61+
            excellent_chances = 0.02;
        } else if recipe_level >= 150 {
            // 60+
            excellent_chances = 0.01;
        } else {
            excellent_chances = 0.02;
        }

        let mut states_and_rates: Vec<(StepState, f64)> = vec![(StepState::Good, good_chances)];

        if self.recipe.expert {
            // TODO proper rates for expert recipes
            states_and_rates.extend(vec![
                (StepState::Centered, 0.25),
                (StepState::Sturdy, 0.25),
                (StepState::Pliant, 0.25),
            ]);
        } else {
            states_and_rates.push((StepState::Excellent, excellent_chances));
        }

        // If none of the states actually proc, fallback to normal.
        states_and_rates.push((StepState::Normal, 1.0));

        let mut rnd = rand::thread_rng();
        for (s_state, rate) in states_and_rates {
            let roll = rnd.gen_range(0.0, 1.0);
            if roll <= rate {
                state.current_step_state = s_state;
                break;
            }
        }
    }

    fn get_hq_percent(&self, state: &SimulationState) -> u8 {
        let quality_percent = (state.quality as f64 / self.recipe.quality as f64).min(1.0) * 100.0;

        return if quality_percent == 0.0 {
            1
        } else if quality_percent >= 100.0 {
            100
        } else {
            HQ_TABLE[quality_percent.floor() as usize]
        };
    }
}

/// FFXIV Craft simulator initialization helper
pub struct CraftSimulatorBuilder {
    sim: CraftSimulation,
}

impl CraftSimulatorBuilder {
    /// Create new Craft simulator builder with empty state
    pub fn new(recipe: Recipe, crafter: CrafterStats) -> CraftSimulatorBuilder {
        CraftSimulatorBuilder {
            sim: CraftSimulation::new(recipe, crafter),
        }
    }

    /// Add single action to the end of the craft sim list
    pub fn with_action(mut self, action: CraftingAction) -> CraftSimulatorBuilder {
        self.sim.actions.push(action.create());
        self
    }

    /// Add list of actions to the end of the craft sim list
    pub fn with_actions(mut self, actions: Vec<CraftingAction>) -> CraftSimulatorBuilder {
        self.sim
            .actions
            .extend(actions.iter().map(|action| action.create()));
        self
    }

    // TODO Understand and improve linear description
    /// Should everything be linear (aka no fail on actions, Initial preparations never procs)
    pub fn linear(mut self, linear: bool) -> CraftSimulatorBuilder {
        self.sim.linear = linear;
        self
    }
    // TODO Improve safe_mode description
    /// Safe mode makes all the actions that have success chances < 100 to fail
    pub fn safe_mode(mut self, safe_mode: bool) -> CraftSimulatorBuilder {
        self.sim.safe_mode = safe_mode;
        self
    }

    /// Finish building and return initialized FFXIV Craft Simulation engine
    pub fn simulate(self) -> CraftResult {
        self.sim.run()
    }
}
