use crate::ffxiv::enums::crafting_action::CraftingAction;

pub struct ByregotsBlessing {
    action: CraftingAction,
}

impl ByregotsBlessing {
    pub fn new() -> ByregotsBlessing {
        ByregotsBlessing {
            action: CraftingAction::ByregotsBlessing,
        }
    }
}
//
// impl ByregotsBlessing {
//     fn get_info(&self) -> CraftingAction {
//         self.action
//     }
//
//     fn get_level_requirement(&self, state: &CraftState) -> (CraftingJob, u8) {
//         (CraftingJob::ANY, 50)
//     }
// }
