use crate::ffxiv::enums::crafting_action::CraftingAction;

pub struct BasicSynthesis {
    action: CraftingAction,
}

impl BasicSynthesis {
    pub fn new() -> BasicSynthesis {
        BasicSynthesis {
            action: CraftingAction::BasicSynthesis,
        }
    }
}
//
// impl ActionLogic for BasicSynthesis {
//     fn get_info(&self) -> CraftingAction {
//         self.action
//     }
//
//     fn get_level_requirement(&self, state: &CraftState) -> (CraftingJob, u8) {
//         (CraftingJob::ANY, 5)
//     }
// }
