use crate::ffxiv::enums::crafting_job::CraftingJob;

pub struct Recipe {
    pub id: i32,
    pub rlvl: u16,
    pub job: CraftingJob,
    pub durability: u16,
    pub quality: u16,
    pub progress: u16,
    pub control_required: u16,
    pub craftsmanship_required: u16,

    pub lvl: u32,
    pub suggested_craftsmanship: u32,
    pub suggested_control: u32,
    pub stars: Option<u32>,
    pub hq: bool,
    pub quick_synth: bool,

    // pub unlock_id: Option<u32>,
    // pub ingredients: Vec<Ingredient>,
    // pub yields: Option<u32>,
    pub expert: bool,
}

pub struct CrafterStats {
    pub job_id: u32,
    pub craftsmanship: u32,
    pub _control: u32,
    pub cp: i32,
    pub specialist: bool,
    pub level: u32,
    // pub levels: [Option<u32>; 8],
}
