#[derive(PartialEq, Clone, Copy, Eq, Hash)]
pub enum Buff {
    InnerQuiet,
    WasteNot,
    WasteNot2,
    Manipulation,
    GreatStrides,
    Innovation,
    Veneration,
    MakersMark,
    NameOfTheElements,
    MuscleMemory,
    FinalAppraisal,
    /// Name of the elements cannon be applied anymore
    Nameless,
}
