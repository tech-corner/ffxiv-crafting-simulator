#[derive(PartialEq, Copy, Clone)]
pub enum StepState {
    Normal,
    Good,
    Excellent, // Not available on expert recipes
    Poor,      // Not available on expert recipes
    // Only for expert recipes
    Centered, // Increase success rate by 25%
    Sturdy,   // Reduces loss of durability by 50%, stacks with WN & WN2
    Pliant,   // Reduces CP cost by 50%
    // Fails the step
    Failed,
}
