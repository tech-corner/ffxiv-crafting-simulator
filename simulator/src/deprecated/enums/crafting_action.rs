use crate::ffxiv::logic::craft_simulation::SimulationState;
use crate::{
    ffxiv::enums::fail_cause::FailCause, ffxiv::logic::action::Action,
    ffxiv::logic::craft_simulation::CraftSimulation,
};

#[derive(PartialEq, Copy, Clone)]
pub enum CraftingAction {
    // Buff
    FinalAppraisal,
    GreatStrides,
    InnerQuiet,
    Innovation,
    Manipulation,
    NameOfTheElements,
    Veneration,
    WasteNot,
    WasteNotII,
    // Other
    DelicateSynthesis,
    MasterMend,
    Observe,
    RemoveFinalAppraisal,
    TrickOfTheTrade,
    // Progression
    BasicSynthesis,
    BrandOfTheElements,
    CarefulSynthesis,
    Groundwork,
    IntensiveSynthesis,
    MuscleMemory,
    RapidSynthesis,
    // Quality
    BasicTouch,
    ByregotsBlessing,
    FocusedTouch,
    HastyTouch,
    PatientTouch,
    PreciseTouch,
    PreparatoryTouch,
    PrudentTouch,
    Reflect,
    StandardTouch,
    TrainedEye,
}

impl CraftingAction {
    pub fn create(&self) -> Box<dyn Action> {
        match self {
            CraftingAction::BasicSynthesis => Box::new(SuccessfulAction {}),
            CraftingAction::BasicTouch => Box::new(FailedAction {}),
            _ => unimplemented!(),
        }
    }
}

struct SuccessfulAction {}
impl Action for SuccessfulAction {
    fn get_type(&self) -> CraftingAction {
        CraftingAction::BasicSynthesis
    }

    fn success_rate(&self, sim: &CraftSimulation, state: &SimulationState) -> u8 {
        0
    }

    fn requires_good(&self) -> bool {
        false
    }

    fn get_durability_cost(&self, sim: &CraftSimulation, state: &SimulationState) -> u16 {
        0
    }

    fn get_cp_cost(&self, sim: &CraftSimulation, state: &SimulationState) -> u16 {
        0
    }

    fn will_fail(&self, sim: &CraftSimulation, state: &SimulationState) -> Option<FailCause> {
        Option::None
    }

    fn skip_on_fail(&self) -> bool {
        false
    }

    fn skip_buff_ticks(&self) -> bool {
        false
    }

    fn execute(&self, sim: &CraftSimulation, state: &mut SimulationState) {}

    fn on_fail(&self, sim: &CraftSimulation, state: &mut SimulationState) {}
}

struct FailedAction {}
impl Action for FailedAction {
    fn get_type(&self) -> CraftingAction {
        CraftingAction::BasicTouch
    }

    fn success_rate(&self, sim: &CraftSimulation, state: &SimulationState) -> u8 {
        0
    }

    fn requires_good(&self) -> bool {
        false
    }

    fn get_durability_cost(&self, sim: &CraftSimulation, state: &SimulationState) -> u16 {
        0
    }

    fn get_cp_cost(&self, sim: &CraftSimulation, state: &SimulationState) -> u16 {
        0
    }

    fn will_fail(&self, sim: &CraftSimulation, state: &SimulationState) -> Option<FailCause> {
        Option::Some(FailCause::MissingLevelRequirement)
    }

    fn skip_on_fail(&self) -> bool {
        false
    }

    fn skip_buff_ticks(&self) -> bool {
        false
    }

    fn execute(&self, sim: &CraftSimulation, state: &mut SimulationState) {}

    fn on_fail(&self, sim: &CraftSimulation, state: &mut SimulationState) {}
}
