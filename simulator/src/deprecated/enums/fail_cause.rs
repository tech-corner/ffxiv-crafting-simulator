#[derive(Copy, Clone)]
pub enum FailCause {
    UnsafeAction,
    DurabilityReachedZero,
    NotEnoughCP,
    MissingLevelRequirement,
    MissingStatsRequirement,
    NotSpecialist,
    NoInnerQuite,
}
