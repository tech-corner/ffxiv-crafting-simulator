pub struct Recipe {
    pub id: u16,
    pub lvl: u8,
    pub rlvl: u16,
    pub job_id: u8,

    pub durability: u8,
    pub progress: u16,
    pub quality: u32,

    pub control_required: u16,
    pub craftsmanship_required: u16,

    pub suggested_craftsmanship: u16,
    pub suggested_control: u16,

    pub stars: Option<u8>,
    pub hq: bool,
    pub quick_synth: bool,
    pub expert: bool,
}