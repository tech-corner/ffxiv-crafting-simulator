use crate::ffxiv::enums::crafting_job::CraftingJob;

pub struct CrafterStats {
    pub job: CraftingJob,
    pub craftsmanship: u16,
    pub control: u16,
    pub cp: u16,
    pub specialist: bool,
    pub level: u8,
}
