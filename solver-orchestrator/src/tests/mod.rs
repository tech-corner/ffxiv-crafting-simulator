use crate::orchestrator::Orchestrator;
use ffxiv_crafting_simulator::ffxiv::data::crafter_stats::CrafterStats;
use ffxiv_crafting_simulator::ffxiv::enums::crafting_job::CraftingJob;
use ffxiv_crafting_simulator::ffxiv::data::recipe::Recipe;

#[test]
fn create() {
    let stats = CrafterStats {
        job: CraftingJob::CRP,
        level: 80,
        control: 1884,
        craftsmanship: 1926,
        cp: 439,
        specialist: false
    };
    let recepie = Recipe {
        id: 33055,
        lvl: 70,
        rlvl: 350,
        job_id: 8,
        durability: 70,
        progress: 2760,
        quality: 13144,
        control_required: 1350,
        craftsmanship_required: 1500,
        suggested_craftsmanship: 1500,
        suggested_control: 1350,
        stars: Option::Some(3),
        hq: false,
        quick_synth: true,
        expert: false
    };
    let solver: Orchestrator =  Orchestrator::new(stats, recepie);
}
