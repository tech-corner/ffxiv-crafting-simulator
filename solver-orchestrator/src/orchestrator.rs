use ffxiv_crafting_simulator::ffxiv::data::crafter_stats::CrafterStats;
use ffxiv_crafting_simulator::ffxiv::data::recipe::Recipe;

pub struct Orchestrator {}

impl Orchestrator {
    pub fn new(stats: CrafterStats, recipe: Recipe) -> Orchestrator {
        Orchestrator {}
    }
}
